<?php
/**
 * Joomla-Komponente - Tierverwaltung
 *
 * Einstiegspunkt im Backend
 *
 * @package           Animals
 * @subpackage      Backend
 * @author              Thomas Wenzel
 * @license             GNU/GPL
 */
defined('_JEXEC') or die;
JLoader::import('joomla.application.component.controller');

/* Einstieg in die Komponente - AnimalsController instanziieren */
$controller = JController::getInstance('animals');

/* Das Anwendungsobjekt holen  */
$app = JFactory::getApplication();

/* Aufgabe (task) ausführen. Hier ist das die Ausgabe der Standardview */
$controller->execute($app->input->get('task'));

/* Dialogsteuerung */
$controller->redirect();
