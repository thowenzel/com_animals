<?php
/**
 * Allgemeiner Controller der Komponente animals
 *
 * @package          animals
 * @subpackage     Backend
 * @author              Thomas Wenzel
 * @license             GNU/GPL
 */
defined('_JEXEC') or die;
JLoader::import('joomla.application.component.controller');

/* Helperklasse dem JLoader melden, bei Bedarf wird sie schnell geladen */
JLoader::register('AnimalsHelper', JPATH_COMPONENT . '/helpers/animals.php');

/**
 * Erweiterung der Basisklasse JController
 */
class animalsController extends JController
{
	/**
	 * @var string Standardview
	 */
	protected $default_view = 'animals';

	/**
	 * Ausgabe der View animals.
	 * @inherit
	 */
	public function display($cachable = false, $urlparams = false)
	{
		/* @var $input JInput Unsere Einnahmequelle */
		$input = JFactory::getApplication()->input;

		// alle Variablen mit Vorgabewerten initialisieren
		$view   = $input->get('view', $this->default_view);
		$layout = $input->get('layout', 'default');
		$id     = $input->get('id');
        
        // Bevor die View aufgebaut wird, erstellt die Helperklasse
        // ein Untermenü zum Wechseln zwischen Categories und Animals
        AnimalsHelper::addSubmenu($view);

		if ($view == 'animal' && $layout == 'edit')
		{
			// checkEditId() ist eine Methode von JController, die den Kontext prüft
			if (!$this->checkEditId('com_animals.edit.animal', $id)) {
				// Kommentarlos zurück zur default-view
				$this->setRedirect(JRoute::_('index.php?option=com_animals&view=animals', false));
				return false;
			}
		}

		// Alles geprüft und ok, die View kann ausgegeben werden
		parent::display($cachable, $urlparams);
	}
}
