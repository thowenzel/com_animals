#
# Tabelle `#__animals`
# Basistabelle fuer die Verwaltung der Tiere
#

DROP TABLE IF EXISTS `#__animals`;
CREATE TABLE IF NOT EXISTS `#__animals` (
    `id` int(10) NOT NULL AUTO_INCREMENT,
    `created` datetime DEFAULT '0000-00-00',
    `created_by_id`  int(11) DEFAULT NULL,
    `name` varchar(255) DEFAULT NULL,
    `species_id` int(11) DEFAULT NULL,
    `subspecies` varchar(255) DEFAULT NULL,
    `color` varchar(255) DEFAULT NULL,
    `birthday` int(10) DEFAULT NULL,
    `sex` varchar(20) DEFAULT NULL,
    `zur_vermittlung` tinyint(1) DEFAULT NULL,
    `is_castrated` tinyint(1) DEFAULT NULL,
    `is_chiped` tinyint(1) DEFAULT NULL,
    `has_tattoo` tinyint(1) DEFAULT NULL,
    `main_picture` varchar(100) DEFAULT NULL,
    `more_pictures` text DEFAULT NULL,
    `description` mediumtext DEFAULT NULL,
    `additional_info` mediumtext DEFAULT NULL,
    `success_story` mediumtext DEFAULT NULL,
    `rainbow_story` mediumtext DEFAULT NULL,
    `state` varchar(50) DEFAULT NULL,
    `vermittelt` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

