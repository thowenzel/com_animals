<?php
/**
 * Joomla-Komponente zur Verwaltung der zur Vermittlung stehenden Tiere
 *
 * Detailansicht von com_animals im Frontend.
 * @package        Frontend
 * @subpackage     com_animals
 * @author         Thomas Wenzel
 * @license        GNU/GPLv2 or later
 */
defined('_JEXEC') or die;

/* Import der Basisklasse "JView" */
jimport('joomla.application.component.view');

/**
 * Erweiterung der Basisklasse "JView" zur Anzeige der Detailansicht
 */
class AnimalsViewAnimal extends JView
{
	/* Der Datensatz */
    protected $item;

	/**
	* Überschreiben der Methode "display"
	*
	* @param string $tpl alternative Layoutdatei, leer = 'default'
	*/
    function display($tpl = null)
    {
	    /* getItem() aus JModelList aufrufen */
        $this->item	= $this->get('Item');

        /* View ausgeben - zurückdelegiert an die Elternklasse */
        parent::display($tpl);
    }
}
