<?php
/**
 * Joomla-Komponente zur Verwaltung der zur Vermittlung stehenden Tiere
 *
 * Kategorie-Ansicht com_animals im Frontend (Anzeige aller Datensaetze einer Kategorie).
 * @package           Frontend
 * @subpackage      com_animals
 * @author               Thomas Wenzel
 * @license             GNU/GPLv2 or later
 */
defined('_JEXEC') or die;

/* Import der Basisklasse JView */
jimport('joomla.application.component.view');

/**
 * Erweiterung der Basisklasse JView
 */
class AnimalsViewSuccess extends JView
{
  /**
   * Die Daten aus der Tabelle
   * @var object $items
   */
  protected $items;
  /**
   * Überschreiben der Methode display
   *
   * @param string $tpl Alternative Layoutdatei, leer = 'default'
   */
  function display($tpl = null)
  {

    /* Die Datensätze mit getItems() aus JModelList aufrufen */
    $this->items = $this->get('Items');

    /* View ausgeben - zurückdelegiert an die Elternklasse */
    parent::display($tpl);
  }
}
