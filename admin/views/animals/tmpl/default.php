<?php
/**
 * Tabellarische Ansicht für com_animals
 *
 * @package         animals
 * @subpackage    Backend
 * @author            Thomas Wenzel
 * @license           GNU/GPL
 */
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');

$nullDate = JFactory::getDbo()->getNullDate();

/* Nach dieser Spalte wird die Tabelle sortiert */
$listOrder = $this->escape($this->state->get('list.ordering'));

/* Sortierrichtung festlegen */
$listDirn = $this->escape($this->state->get('list.direction'));
?>

<form action="<?php echo JRoute::_('index.php?option=com_animals&view=animals'); ?>"
      method="post" name="adminForm" id="adminForm">
      
    <fieldset id="filter-bar">
        <div class="filter-search fltlft">
            <label class="filter-search-lbl">
                <?php echo JText::_('JSEARCH_FILTER_LABEL'); ?>
            </label>
            <input type="text" name="filter_search" id="filter_search"
                    value="<?php echo $this->escape($this->state->get('filter.search')); ?>"
                    title="<?php echo JText::_('COM_ANIMALS_SEARCH'); ?>"/>
            <button type="submit"><?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?></button>
            <button type="button" onclick="document.id('filter_search').value='';this.form.submit();">
                    <?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>
            </button>
        </div>
    </fieldset>
        
    <div class="clr"></div>
      
    <table class="adminlist">
        <thead>
        <tr>
            <th width="5%">
                <input type="checkbox" name="checkall-toggle" value="" onclick="checkAll(this)"/>
            </th>
            <th width="5%"><?php echo JText::_('COM_ANIMALS_ID'); ?></th>
            <th width="10%"><?php echo JHtml::_('grid.sort', 'COM_ANIMALS_NAME_LABEL', 'name', $listDirn, $listOrder); ?></th>
            <th width="10%"><?php echo JHtml::_('grid.sort', 'COM_ANIMALS_SPECIES_LABEL', 'category_id', $listDirn, $listOrder); ?>
                <br />
                <select name="filter_species_id" class="inputbox" onchange="this.form.submit()">
                    <option value=""><?php echo JText::_('JOPTION_SELECT_CATEGORY');?></option>
                    <?php echo JHtml::_('select.options', JHtml::_('category.options', 'com_animals'), 'value', 'text', $this->state->get('filter.species_id'));?>
                </select>
             </th>
            
            <th width="10%"><?php echo JHtml::_('grid.sort', 'COM_ANIMALS_SUBSPECIES_LABEL', 'subspecies', $listDirn, $listOrder); ?></th>
            <th width="10%"><?php echo JText::_('COM_ANIMALS_FARBE_LABEL'); ?></th>
            <th width="5%"><?php echo JText::_('COM_ANIMALS_SEX_LABEL'); ?></th>
            <th width="5%"><?php echo JText::_('COM_ANIMALS_GEBURTSDATUM_LABEL'); ?></th>
            <th width="10%"><?php echo JText::_('COM_ANIMALS_STATE_LABEL'); ?></th>
            <th width="10%"><?php echo JText::_('COM_ANIMALS_VERMITTELT'); ?></th>
        </tr>
        </thead>
        
        <tfoot>
            <tr>
                <td colspan="13"><?php echo $this->pagination->getListFooter(); ?></td>
            </tr>
        </tfoot>
        
        <tbody>
    <?php foreach ($this->items as $i => $item) : ?>
        <tr class="row<?php echo $i % 2; ?>">
            <td class="center"><?php echo JHtml::_('grid.id', $i, $item->id); ?></td>
            <td class="center"><?php echo (int)$item->id; ?></td>
            <td><?php
                /* Link zum Formular */
                $mylink = JRoute::_("index.php?option=com_animals&task=animal.edit&id=" . $item->id);
                echo '<a href="' . $mylink . '">' . $this->escape($item->name) . '</a>';
                ?>
            </td>
            <td><?php echo $this->escape($item->species); ?></td>
            <td><?php echo $this->escape($item->subspecies); ?></td>
            <td><?php echo $this->escape($item->color); ?></td>
            <td><?php echo $this->escape($item->sex); ?></td>
            <td><?php echo $this->escape($item->birthday); ?></td>
            <td><?php 
                switch ($item->state) {
                case '0':
                    echo JText::_('COM_ANIMALS_STATE_FOR_MEDIATION');
                break;
    
                case '1':
                    echo JText::_('COM_ANIMALS_STATE_IS_RESERVED');
                break;
    
                case '2':
                    echo JText::_('COM_ANIMALS_STATE_WAS_MEDIATED');
                break;

                default:
                    echo JText::_('COM_ANIMALS_STATE_UNKOWN');
                break;
                } ?>
            </td>
            <td><?php
                if ($this->escape($item->vermittelt) != $nullDate) {
                    echo JHtml::_('date', $item->vermittelt, JText::_('DATE_FORMAT_LC4'));
                }
             ?></td>
        </tr>
    <?php endforeach; ?>
        </tbody>
    </table>

    <input type="hidden" name="task"/>
    <input type="hidden" name="boxchecked" value="0"/>
    <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>"/>
    <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>"/>
    <?php echo JHtml::_('form.token'); ?>
</form>
