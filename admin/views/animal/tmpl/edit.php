<?php
/**
 * HTML-Formular zum Anlegen und Bearbeiten eines Datensatzes.
 *
 * @package         com_animals
 * @subpackage    Backend
 * @author            Thomas Wenzel
 * @license           GNU/GPL
 */
defined('_JEXEC') or die;

// Include the component HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');

// lädt JavaScript-Helfer für Tooltips, Eingabeprüfung
// und zum Aufrechterhalten der Session bei Untätigkeit,
// um Datenverluste während dem Bearbeiten zu vermeiden.
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
?>

<form action="<?php echo JRoute::_('index.php?option=com_animals&id=' . (int)$this->item->id); ?>" method="post" name="adminForm" id="animal-form" class="form-validate">

    <div class="width-60 fltlft">
        <fieldset class="adminform">
        <legend><?php echo JText::_('COM_ANIMALS_STECKBRIEF_SET'); ?></legend>
            <ul class="adminformlist">
                <li>
                    <?php echo $this->form->getLabel('name'); ?>
                    <?php echo $this->form->getInput('name'); ?>
                </li>
                <li>
                    <?php echo $this->form->getLabel('species_id'); ?>
                    <?php echo $this->form->getInput('species_id'); ?>
                </li>
                <li>
                    <?php echo $this->form->getLabel('subspecies'); ?>
                    <?php echo $this->form->getInput('subspecies'); ?>
                </li>
                <li>
                    <?php echo $this->form->getLabel('color'); ?>
                    <?php echo $this->form->getInput('color'); ?>
                </li>
                <li>
                    <?php echo $this->form->getLabel('sex'); ?>
                    <?php echo $this->form->getInput('sex'); ?>
                </li>
                <li>
                    <?php echo $this->form->getLabel('birthday'); ?>
                    <?php echo $this->form->getInput('birthday'); ?>
                </li>
                <li>
                    <?php echo $this->form->getLabel('is_castrated'); ?>
                    <?php echo $this->form->getInput('is_castrated'); ?>
                </li>
                <li>
                <?php echo $this->form->getLabel('main_picture'); ?>
                <?php echo $this->form->getInput('main_picture'); ?>
                </li>
                <li>
                <?php echo $this->form->getLabel('state'); ?>
                <?php echo $this->form->getInput('state'); ?>
                </li>
            </ul>

            <div class="clr"></div>
                <?php echo $this->form->getLabel('description'); ?>
            <div class="clr"></div>
                <?php echo $this->form->getInput('description'); ?>
        </fieldset>
    </div>

    <div class="width-40 fltrt">
        <?php echo JHtml::_('sliders.start', 'content-sliders-'.$this->item->id, array('useCookie'=>1)); ?>
        <?php // Do not show the publishing options if the edit form is configured not to. ?>
        <?php echo JHtml::_('sliders.panel', JText::_('COM_ANIMALS_FIELDSET_METADATA'), 'publishing-details'); ?>
            <fieldset class="panelform">
                <ul class="adminformlist">
                    <li><?php echo $this->form->getLabel('created_by_id'); ?>
                    <?php echo $this->form->getInput('created_by_id'); ?></li>

                    <li><?php echo $this->form->getLabel('created'); ?>
                    <?php echo $this->form->getInput('created'); ?></li>
				</ul>
			</fieldset>
		<?php echo JHtml::_('sliders.end'); ?>
	</div>
    
    
    <div>
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="return" value="<?php echo JRequest::getCmd('return');?>" />
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>