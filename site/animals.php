<?php
/**
 * Joomla-Komponente zur Verwaltung der zur Vermittlung stehenden Tiere
 *
 * Einstiegspunkt im Frontend
 * @package        Frontend
 * @subpackage     com_animals
 * @author         Thomas Wenzel
 * @license        GNU/GPL
 */
defined('_JEXEC') or die;
jimport('joomla.application.component.controller');

$base = JURI::base(true).'/media/com_animals/';

/* Einstieg in die Komponente - AnimalsController instanziieren */
$controller	= JController::getInstance('animals');

/* Import CSS and scripts */

$doc = JFactory::getDocument();

$doc->addScript('//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js');
$doc->addScript($base.'js/jquery.fitvids.js');
$doc->addScript($base.'js/jquery.bxslider.js');

$doc->addScript($base.'js/animals_scripts.js');

$doc->addStyleSheet($base.'css/jquery.bxslider.css');

$doc->addStyleSheet($base.'css/animalsdb.css');

/* Eingabe des Applikationsobjekts besorgen */
$input = JFactory::getApplication()->input;

/* Aufgabe (task) ausführen. Hier ist das die Ausgabe des Standardviews */
$controller->execute($input->get('task'));

/* Dialogsteuerung */
$controller->redirect();