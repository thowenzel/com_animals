<?php
/**
 * Joomla-Komponente zur Verwaltung der zur Vermittlung stehenden Tiere
 *
 * Layout für Ausgabe der Erfolgsgeschichten aller vermittelten Tiere
 *
 * @package           Frontend
 * @subpackage      com_animals
 * @author              Thomas Wenzel
 * @license             GNU/GPLv2 or later
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers');

/* Das Null-Datum der Datenbank, als Vergleichswert */
$nullDate = JFactory::getDbo()->getNullDate();
?>

<div class="introtext">
    <h1>Tiere im Glück</h1>
    <p>An dieser Stelle gibt es nur positive Geschichten..</p>
</div><!--#introtext-->


<?php if ($this->items) { ?>
    <div id="animalsList">
        <?php foreach ($this->items as $item) : ?>
            <div class="animalsListRow">
            
                <div class="imageBox">
                    <img src="<?php echo $item->main_picture; ?>" alt="<?php echo $item->name; ?>" width="200px"/>
                </div><!--#imageBox-->
                
                <div class="textBox">
                    <ul>
                        <li class="name"><h1><?php echo $item->name; ?></h1></li>
                        <li class="rainbow_story"><?php echo $item->success_story; ?></li>
                    </ul>
                </div><!--#textBox-->

            </div><!--#animalsListRow-->
        <?php endforeach;?>
    </div><!--#animalsList-->
<?php } ?>

</div>
