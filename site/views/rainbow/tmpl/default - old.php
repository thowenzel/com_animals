<?php
/**
 * Joomla-Komponente zur Verwaltung der zur Vermittlung stehenden Tiere
 *
 * Layout für Ausgabe aller verstorbenen Tiere
 *
 * @package           Frontend
 * @subpackage      com_animals
 * @author              Thomas Wenzel
 * @license             GNU/GPLv2 or later
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers');

/* Das Null-Datum der Datenbank, als Vergleichswert */
$nullDate = JFactory::getDbo()->getNullDate();
?>

<div class="introtext">
    <h1>Unvergessen</h1>
    <p>Nicht alle Tiere, die zu uns ins Tierheim kommen, haben auch das Glück und finden eine neue Bleibe. Manchmal kommt es vor, das Tiere dauerhaft bei uns bleiben und ihren Lebensabend im Tierheim verbringen.</p>
    <p>Dieser Bereich ist den im Tierheim verstorbenen Tieren gewidmet.</p>
</div><!--#introtext-->


<?php if ($this->items) { ?>
    <div id="animalsList">
        <?php foreach ($this->items as $item) : ?>
            <div class="animalsListRow">
            
                <div class="imageBox">
                    <img src="<?php echo $item->main_picture; ?>" alt="<?php echo $item->name; ?>" width="200px"/>
                </div><!--#imageBox-->
                
                <div class="textBox">
                    <ul>
                        <li class="name"><h1><?php echo $item->name; ?></h1></li>
                        <li class="rainbow_story"><?php echo $item->rainbow_story; ?></li>
                    </ul>
                </div><!--#textBox-->

            </div><!--#animalsListRow-->
        <?php endforeach;?>
    </div><!--#animalsList-->
<?php } ?>

</div>
