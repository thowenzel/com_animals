<?php
/**
 * Joomla-Komponente zur Verwaltung der zur Vermittlung stehenden Tiere
 *
 * Detail-Layout für das Frontend. Ansicht für ein einzelnes Tier.
 * @package         Frontend
 * @subpackage      com_animals
 * @author          Thomas Wenzel
 * @license         GNU/GPLv2 or later
 * 
 * @todo  HTML-Ausgabe escapen
 */
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

/* Das Item, Eigenschaft der View, die hier ausgegeben wird */
$item = $this->item;

/* Das Null-Datum der Datenbank, als Vergleichswert */
$nullDate = JFactory::getDbo()->getNullDate();

/* Verzeichnis mit den Mediendateteien auslesen (es wird der Pfad von "main_picture" verwendet) */

$path_parts = pathinfo($item->main_picture);

$this->signage_Path = $path_parts['dirname'];
$mediafiles = scandir($this->signage_Path);
?>

<div id="animaldetailed">

  <h1><?php echo $item->name; ?></h1>
  <p><?php echo $item->subspecies; ?></p>
  <p>Farbe: <?php echo $item->color; ?></p>
  <p>Geschlecht: <?php echo $item->sex; ?></p>
  <p>Alter: <?php echo $item->birthday; ?></p>

  <div id="animaldetailed_galery">

    <ul class="bxslider">
        <?php
            foreach ($mediafiles as $mediafile) { // Ausgabeschleife
                $fileinfo = pathinfo($this->signage_Path."/".$mediafile);
                if ($mediafile != "." && $mediafile != "..") { // Nur Dateien ausgeben
                
                    //Nur Bilddateien sammeln
                    $imagetypes= array("jpg", "jpeg", "gif", "png", "bmp");
                
                    if(in_array($fileinfo['extension'],$imagetypes)) {
        ?>
                    <li>
                        <img src="<?php echo $this->signage_Path."/".$mediafile; ?>" title="<?php echo $item->name; ?>"/>
                    </li>
        <?php
                    };
                };
            };
        ?>
    </ul>

  </div> <!-- animaldetailed_galery -->
  
  <div id="animaldetailed_description">
    <p><?php echo $item->description; ?></p>
  </div> <!-- animaldetailed_description -->
  
  <h2 align="center">
    <?php 
        switch ($item->state) {
            case '0':
                echo JText::_('COM_ANIMALS_STATE_FOR_MEDIATION');
            break;
    
            case '1':
                echo JText::_('COM_ANIMALS_STATE_IS_RESERVED');
            break;
    
            case '2':
                echo JText::_('COM_ANIMALS_STATE_WAS_MEDIATED');
            break;

            default:
                echo JText::_('COM_ANIMALS_STATE_UNKOWN');
            break;
        } ?>
  </h2>
  
  <p align="center">
    <a href="http://www.tierheim-lindau.de/cms/index.php/unsere-tiere/hunde">Zurück zur Übersicht</a>
  </p>
</div> <!-- animaldetailed -->

