<?php
/**
 * Helperklasse für die Komponente animals
 *
 * @package     animals
 * @subpackage  Backend
 * @author      Thomas Wenzel
 * @license     GNU/GPL
 */
defined('_JEXEC') or die;

class AnimalsHelper {

    /**
     * In Listenansicht im Contenbereich ein Submenü aufbauen um Wechsel zwischen 
     * Kategorien und Animals zu ermöglichen.
     *
     * @param type $name
     */
    public static function addSubmenu($name) {

        /* Tab "Tiere" */
        JSubMenuHelper::addEntry(
            JText::_('COM_ANIMALS_SUBMENU_ANIMALS'),
            'index.php?option=com_animals&view=animals', $name == 'animals'
        );

        /* Tab "Tierarten (Kategorien)" */
        JSubMenuHelper::addEntry(
            JText::_('COM_ANIMALS_SUBMENU_CATEGORIES'),
            'index.php?option=com_categories&extension=com_animals', $name == 'categories'
        );

        /*
         * Auf der Listenansicht der Kategorien wird der normale Seitentitel
         * "Kategorien" erweitert mit dem Namen unserer Komponente
         */
        if ($name == 'categories') {
            JToolBarHelper::title(
                JText::sprintf('COM_ANIMALS_CATEGORIES_TITLE',
                    JText::_('com_animals')), 'animals-categories');
        }
    }

    /**
     * Diese Methode baut ein Werteobjekt auf mit den Namen aller
     * Aktionen, die in der access.xml definiert sind und gibt bei
     * jeder Aktion an, ob sie der Benutzer ausführen darf.
     *
     * @param  int $categoryId
     * @return JObject
     */
    public static function getActions($categoryId = 0) {

        /* Der User, der gerade aktiv ist und dessen Berechtigungen ermittelt werden */
        $user = JFactory::getUser();

        /* Falls eine categoryId übergeben wurde, ist sie unser Asset */
        if (empty($categoryId)) {
            $assetName = 'com_animals';
        } else {
            $assetName = 'com_animals.category.' . (int) $categoryId;
        }

        /* Das Ausgabeobjekt wird neu angelegt */
        $result = new JObject;

        /* Die Aktionen einlesen, die bei MyThings möglich sind */
        $actions =  JAccess::getActions('com_animals');

        // Für jede Aktion wird geprüft, ob der User authorisiert ist,
        // diese auszuführen (TRUE/FASLE)
        foreach ($actions as $action) {
            $result->set($action->name, $user->authorise($action, $assetName));
        }

        return $result;
    }

}
