<?php
/**
 * Joomla-Komponente zur Verwaltung der zur Vermittlung stehenden Tiere
 *
 * Datenmodell, Ausgabe aller Datensätze einer Kategorie
 * @package         Frontend
 * @subpackage      com_animals
 * @author          Thomas Wenzel
 * @license         GNU/GPLv2 or later
 */

defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Erweiterung der Basisklasse JModelList
 */
class AnimalsModelCategory extends JModelList
{
  /**
   * Die Methode wird überschrieben, um den Tabellennamen und die
   * benötigten Spalten anzugeben.
   *
   * @return JDatabaseQuery für die Abfrage der Datentabelle
   */
  protected function getListQuery()
  {
    /* Neue JDatabaseQuery für die Abfrage der Datensätze anfordern */
    $db = $this->getDbo();
    $query = $db->getQuery(true);
    
    $catID     = JRequest::getInt('id');

    /* Name der Tabelle für die Komponente */
    $query->from('#__animals AS a');
    
    /* Alle Tabellenspalten der gewählten Species anfordern, aber nur mit Status "Zur Vermittlung (0)" oder "Reserviert (1)" */
    $query->select('a.*');
    $query->where('(species_id = ' . $catID . ' AND (state = 0 OR state = 1))');
    
    /*
     * Kategorienamen zu den species_id aus #__categories über left join ermitteln
     */
    $query->select('c.title AS species');
    $query->join('LEFT', '#__categories AS c ON c.id = a.species_id');

    /* Das Abfrageobjekt zurückgeben */
    return $query;
  }

}
