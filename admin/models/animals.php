<?php
/**
 * Das Model animals liefert Daten für die Übersicht
 *
 * @package       animals
 * @subpackage  Backend
 * @author          Thomas Wenzel
 * @license         GNU/GPL
 */
defined('_JEXEC') or die;
jimport('joomla.application.component.modellist');

/**
 * Erweiterung der Klasse JModelList, abgeleitet von JModel
 */
class animalsModelanimals extends JModelList
{

    /**
    * Konstruktor - legt die Felder fest, die bei Sortierung
    * und Suche verwendet werden
    */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array('id', 'name', 'species_id', 'subspecies', 'created_by_id');
        }

        parent::__construct($config);
    }
    
      /*
   * Ergänzungen zum Setzen des "Datenzustandes" (state) des Models
   * damit der Suchfilter nicht verloren geht. Standard: sortiert
   * nach title, aufsteigend
   *
   * @param string $ordering  Tabellenspalte nach der sortiert wird
   * @param string $direction Sortierrichtung, ASC = aufsteigend
   * @see JModelList::populateState()
   */
    protected function populateState($ordering = 'name', $direction = 'ASC')
    {
        /* Suchbegriff aus vorheriger Eingabe ermitteln */
        $search = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search', '', 'string');

        /* Suchbegriff für diese Ausgabe setzen */
        $this->setState('filter.search', $search);

        /* Auswahl des Benutzers in der Kategorie-Auswahl, übertragen in das state-Objekt */
        $speciesId = $this->getUserStateFromRequest($this->context.'.filter.species_id', 'filter_species_id', '');
        $this->setState('filter.species_id', $speciesId);

        /* Sortieren erledigt die Elternklasse */
        parent::populateState($ordering, $direction);
    }
  
    /**
    * Ident-Schlüssel für den aktuellen Datenzustand anpassen,
    * damit eine gleichzeitige Suche in anderen Fenstern nicht
    * zu Verwechslungen führt.
    *
    * @see JModelList::getStoreId()
    */
    protected function getStoreId($id = '')
    {
        $id	.= ':'.$this->getState('filter.search');
        $id .= ':'.$this->getState('filter.species_id');
        
        return parent::getStoreId($id);
    }

    /**
    * Datenbankabfrage für die Listenansicht aufbauen.
    * Suchfilter und Sortierung werden berücksichtigt, ansonsten
    * wird aufsteigend nach `bezeichnung` sortiert.
    *
    * @return JDatabaseQuery
    */
    protected function getListQuery()
    {
    /* Referenz auf das Datenbankobjekt */
    $db		= $this->getDbo();

    /* Ein neues, leeres JDatabaseQuery-Objekt anfordern */
    $query	= $db->getQuery(true);

    /* Select-Anfrage aufbauen: Basis ist die Tabelle #__animals */
    $query->from('#__animals AS a');
    
    /* Alle Spalten von #__animals auswählen */
    $query->select('a.*' );
    
    /*
     * Benutzernamen zu den created_by_id aus #__users über left join ermitteln
     */
    $query->select('u.username AS created_by');
    $query->join('LEFT', '#__users AS u ON u.id = a.created_by_id');
    
    /*
     * Kategorienamen zu den species_id aus #__categories über left join ermitteln
     */
    $query->select('c.title AS species');
    $query->join('LEFT', '#__categories AS c ON c.id = a.species_id');
    
    /* Auswahl im Kategoriefilter ermitteln */
    $speciesId = $this->getState('filter.species_id');
    
    /*
     * Wurde eine Kategorie gewählt, ist der Wert numerisch
     * Suche einschränken auf diese species_id
     */
    if (is_numeric($speciesId)) {
        $query->where('a.species_id = '.(int) $speciesId);
    }
    
    /* Falls eine Eingabe im Suchfeld steht: Suche ergänzen */
    $search = $this->getState('filter.search');
    
    if (!empty($search)) {

        /* Um code-injection vorzubeugen haben wir hier quote() verwendent */
        $s = $db->quote('%'.$db->escape($search, true).'%');
        $query->where('a.name LIKE '. $s .' OR a.subspecies LIKE '. $s);
    }

    /* Abfrage um die Sortierangaben ergänzen, Standardwert ist angegeben */
    $sort  = $this->getState('list.ordering', 'name');
    $order = $this->getState('list.direction', 'ASC');

    $query->order($db->escape($sort).' '.$db->escape($order));

    /* Fertig ist die Abfrage */
    return $query;
  }
}
