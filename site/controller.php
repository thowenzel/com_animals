<?php
/**
 * Joomla-Komponente zur Verwaltung der zur Vermittlung stehenden Tiere
 *
 * Controller: steuert den Dialog
 * @package        Frontend
 * @subpackage     com_animals
 * @author         Thomas Wenzel
 * @license        GNU/GPL
 */
defined('_JEXEC') or die;

/**
 * Der Controller AnimalsController erbt alles von JController
 */
class AnimalsController extends JController
{
}
