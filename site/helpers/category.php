<?php
/**
 * @package         Joomla.Site
 * @subpackage      com_animals
 * @author          Thomas Wenzel
 * @license         GNU/GPLv2 or later
 */

// no direct access
defined('_JEXEC') or die;
jimport( 'joomla.application.categories' );

/**
 * Category tree for component "com_animals"
 *
 * @static
 * @package       Joomla.Site
 * @subpackage    com_content
 * @since         1.6
 */
class AnimalsCategories extends JCategories
{
	public function __construct($options = array())
	{
		$options['table'] = '#__content';
		$options['extension'] = 'com_animals';
		parent::__construct($options);
	}
}
