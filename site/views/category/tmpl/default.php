<?php
/**
 * Joomla-Komponente zur Verwaltung der zur Vermittlung stehenden Tiere
 *
 * Layout für Ausgabe aller angelegten Datensätze einer gewählen Kategorie
 *
 * @package           Frontend
 * @subpackage      com_animals
 * @author              Thomas Wenzel
 * @license             GNU/GPLv2 or later
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers');

/* Das Null-Datum der Datenbank, als Vergleichswert */
$nullDate = JFactory::getDbo()->getNullDate();
?>

<?php if ($this->items) { ?>
    <div id="animalsList">
        <?php foreach ($this->items as $item) : ?>
            <div class="animalsListRow">
            
                <div class="imageBox">
                    <img src="<?php echo $item->main_picture; ?>" alt="<?php echo $item->name; ?>" width="200px"/>
                </div><!--#imageBox-->
                
                <div class="textBox">
                    <ul>
                        <li class="name"><h1><?php /* Link zur Detailansicht */
                            $link = JRoute::_("index.php?option=com_animals&view=animal&id=" .$item->id);
                            echo '<a href="' .$link .'">' .$item->name .'</a>';
                         ?></h1></li>
                        <li class="art"><?php echo $item->species; ?> / <?php echo $item->subspecies; ?></li>
                        <li class="farbe"><?php echo $item->color; ?></li>
                        <li class="geschlecht"><?php echo $item->sex; ?></li>
                        <li class="kastriert"><?php echo $item->is_castrated; ?></li> <li class="geburtsdatum">Geburtsdatum: <?php echo $item->birthday; ?></li>
                        <li class="detail_link">
                        <?php /* Link zur Detailansicht */
                            $link = JRoute::_("index.php?option=com_animals&view=animal&id=" .$item->id);
                            echo '<a href="' .$link .'">Ausführliche Beschreibung</a>';
                         ?></li>
                    </ul>
                </div><!--#textBox-->

            </div><!--#animalsListRow-->
        <?php endforeach;?>
    </div><!--#animalsList-->
<?php } ?>

</div>
