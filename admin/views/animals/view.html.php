<?php
/**
 * Standard-Ansicht com_animals im Backend.
 *
 * @package    com_animals
 * @subpackage Backend
 * @author     Thomas Wenzel
 * @license    GNU/GPLv2 or later
 */
defined('_JEXEC') or die;

/* Import der Basisklasse JView */
jimport('joomla.application.component.view');

/**
 * Erweiterung der Basisklasse JView
 */
class animalsViewanimals extends JView
{
    /**
    * Die Tabellenzeilen für den mittleren Teil der View
    * @var object $items
    */
    protected $items;
    
    /**
   * Die Daten für die Blätterfunktion
   * @var object $pagination
   */
    protected $pagination;
    
    /**
    * Die Daten der aktuellen Session
    * @var object $state
    */
    protected $state;

    /**
    * Überschreiben der Methode display
    *
    * @param string $tpl Alternative Layoutdatei, leer = 'default'
    */
    function display($tpl = null)
    {
        /* JView holt die Daten vom Model */

        /* Die Datensätze aus der Tabelle animals */
        $this->items = $this->get('Items');
        
        /* Daten für die Blätterfunktion  */
        $this->pagination	= $this->get('Pagination');
    
        /* Statusinformationen für die Sortierung */
        $this->state		= $this->get('State');

		/* Aufnbau der Toolbar */
		$this->addToolbar();

		/* View ausgeben - zurückdelegiert an die Elternklasse */
		parent::display($tpl);
	}

	/**
	 * Aufbau der Toolbar, es werden nur die Buttons eingefügt,
	 * für die der Benutzer eine Berechtigung hat.
	 */
	protected function addToolbar()
	{
		/* Links oben der Titel */
		JToolBarHelper::title(JText::_('COM_ANIMALS_ADMIN'));

		/* Button addNew;  Ein Datensatz, daher Controller animal, task add */
		JToolBarHelper::addNew('animal.add', 'JTOOLBAR_NEW');

		/* Button editList;  Ein Datensatz, daher Controller animal, task edit */
		JToolBarHelper::editList('animal.edit', 'JTOOLBAR_EDIT');

		/* Button delete, kann sich auf mehrere Datensätze beziehen, daher animals */
		JToolBarHelper::deleteList('', 'animals.delete', 'JTOOLBAR_DELETE');

	}

}
