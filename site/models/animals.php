<?php
/**
 * Joomla-Komponente zur Verwaltung der zur Vermittlung stehenden Tiere
 *
 * Datenmodell, Listenausgabe der vorhandenen Daten
 * @package         Frontend
 * @subpackage      com_animals
 * @author          Thomas Wenzel
 * @license         GNU/GPLv2 or later
 */
defined('_JEXEC') or die;

/* Import der Basisklasse "JModelList" */
jimport('joomla.application.component.modellist');

/**
 * Erweiterung der Basisklasse "JModelList"
 */
class AnimalsModelAnimals extends JModelList
{
  /**
   * Die Methode wird überschrieben, um den Tabellennamen und die
   * benötigten Spalten anzugeben.
   *
   * @return JDatabaseQuery für die Abfrage der Datentabelle
   */
  protected function getListQuery()
  {
    /* Neue JDatabaseQuery für die Abfrage der Datensätze anfordern */
    $db = $this->getDbo();
    $query = $db->getQuery(true);

    /* Name der Tabelle für die Komponente */
    $query->from('#__animals AS a');
    
    /* Alle Tabellenspalte anfordern `*/
    $query->select('a.*');
    
    /*
     * Kategorienamen zu den species_id aus #__categories über left join ermitteln
     */
    $query->select('c.title AS species');
    $query->join('LEFT', '#__categories AS c ON c.id = a.species_id');

    /* Das Abfrageobjekt zurückgeben */
    return $query;
  }

}
