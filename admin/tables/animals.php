<?php
/**
* Joomla-Komponente - Tierverwaltung
* Tabelle animals
*
* @package          animals
* @subpackage     Backend
* @author              Thomas Wenzel
* @license             GNU/GPL
*/
defined('_JEXEC') or die;

/**
* Erweiterung der Klasse JTable
*/
class animalsTableanimals extends JTable
{
	/**
	* @var int $id Primärschlüssel
	*/
	public $id;

	/**
	* @var string $name - Der Name des Tiers
	*/
	public $name;
    
    /**
    * @var integer $created_by_id - verweist auf einen Eintrag in #__users
    */
    public $created_by_id;
    
    /**
    * @var integer $species_id - verweist auf einen Eintrag in #__category
    */
    public $species_id;
    
	/**
	* Konstruktor setzt Tabellenname, Primärschlüssel und das
	* übergebene Datenbankobjekt.
	*/
	public function __construct($db)
	{
		parent::__construct('#__animals', 'id', $db);
	}

}
