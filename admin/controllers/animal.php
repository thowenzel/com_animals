<?php
/**
 * Joomla-Komponente - Tierverwaltung
 * 
 * Controller für die View animal (Formular)
 *
 * @package    animals
 * @subpackage Backend
 * @author     Thomas Wenzel
 * @license    GNU/GPL
 */
defined('_JEXEC') or die;
jimport('joomla.application.component.controllerform');

/**
 * Der Controller animalsController erbt alles von JController
 */
class animalsControlleranimal extends JControllerForm
{
}
