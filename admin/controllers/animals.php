<?php
/**
 * Joomla-Komponente - Tierverwaltung
 *
 * Controller für die Listenansicht animals
 *
 * @package    animals
 * @subpackage Backend
 * @author     Thomas Wenzel
 * @license    GNU/GPL
 */
defined('_JEXEC') or die;
jimport('joomla.application.component.controlleradmin');

/**
 * Erweiterung der Klasse JControllerForm
 */
class animalsControlleranimals extends JControllerAdmin
{
  /**
   * Verbindung zu animalsModelanimal, damit die dort
   * enthaltenen Methoden zum Lesen von Datensätzen
   * verwendet werden können.
   *
   * @return animalsModelanimals Das Model für die Listenansicht
   */
  public function getModel($name = 'animal', $prefix = 'animalsModel', $config = array())
  {
    // Model nicht automatisch mit Inhalten aus dem Request befüllen
    $config['ignore_request'] = true;

    // restliche Arbeit der Elternklasse überlassen
    return parent::getModel($name, $prefix, $config);
  }

}
