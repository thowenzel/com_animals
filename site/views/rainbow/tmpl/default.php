<?php
/**
 * Joomla-Komponente zur Verwaltung der zur Vermittlung stehenden Tiere
 *
 * Layout für Ausgabe aller verstorbenen Tiere
 *
 * @package           Frontend
 * @subpackage      com_animals
 * @author              Thomas Wenzel
 * @license             GNU/GPLv2 or later
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers');

/* Das Null-Datum der Datenbank, als Vergleichswert */
$nullDate = JFactory::getDbo()->getNullDate();
?>

<div class="introtext">
    <h1>Unvergessen</h1>
    <p>Nicht alle Tiere, die zu uns ins Tierheim kommen, haben auch das Glück und finden eine neue Bleibe. Manchmal kommt es vor, das Tiere dauerhaft bei uns bleiben und ihren Lebensabend im Tierheim verbringen.</p>
    <p>Dieser Bereich ist den im Tierheim verstorbenen Tieren gewidmet.</p>
</div><!--#introtext-->


<?php if ($this->items) { ?>
    <div id="animalsList">
        <?php foreach ($this->items as $item) : ?>
            <div class="animalsListRow">
            
                <div class="imageBox">
                    <img src="<?php echo $item->main_picture; ?>" alt="<?php echo $item->name; ?>" width="200px"/>
                </div><!--#imageBox-->
                
                <div class="textBox">
                    <ul>
                        <li class="name"><h1><?php echo $item->name; ?></h1></li>
                        <li class="story_intro">
                            <?php if (empty($item->rainbow_story_intro)) {
                                echo $item->name." ist leider verstorben.";
                            } else {
                                echo $item->rainbow_story_intro;
                            } ?>
                        </li>
                        
                        <?php if (!empty($item->rainbow_story_long)) { ?>
                        <li class="detail_link">
                        
                            <?php if (!empty($item->rainbow_story_long)) {
                                $link = JRoute::_("index.php?option=com_animals&view=animal&id=" .$item->id);
                                echo '<a href="' .$link .'">Ausführliche Beschreibung</a>';
                            } ?>
                            
                        </li>
                        <?php } ?>
                        
                    </ul>
                </div><!--#textBox-->

            </div><!--#animalsListRow-->
        <?php endforeach;?>
    </div><!--#animalsList-->
<?php } ?>

</div>
